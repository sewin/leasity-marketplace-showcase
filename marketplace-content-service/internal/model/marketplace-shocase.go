package model

import (
	"encoding/json"
	"strings"

	"github.com/tidwall/gjson"
)

type AdminEntryCard struct {
	Id                  string      `json:"id"`
	Title               string      `json:"title"`
	Subtitle            string      `json:"subtitle,omitempty"`
	Description         string      `json:"description,omitempty"`
	ProposedPhrase      string      `json:"proposedPhrase,omitempty"`
	WebPage             string      `json:"webPage"`
	Logo                string      `json:"logo"`
	Services            interface{} `json:"services"`
	CoverageArea        interface{} `json:"coverageArea"`
	Promotions          interface{} `json:"promotions,omitempty"`
	ConditionPromotions string      `json:"conditionPromotions,omitempty"`
	ExperienceQuality   bool        `json:"experienceQuality"`
	Questions           string      `json:"questions"`
	ContactName         string      `json:"contactName"`
	Email               string      `json:"email"`
	Phone               string      `json:"phone"`
}

type KeyName struct {
	Key  string `json:"key"`
	Name string `json:"name"`
}

func GetAdminCardFromJson(data string) (AdminEntryCard, error) {
	AEC := AdminEntryCard{}
	AEC.Id = gjson.Get(data, "id").String()
	admindata := gjson.Get(data, "attributes").String()
	AEC.Title = gjson.Get(admindata, "CompanyName").String()
	AEC.Subtitle = gjson.Get(admindata, "Subtitle").String()
	AEC.Description = gjson.Get(admindata, "ServicesDescription").String()
	AEC.ProposedPhrase = gjson.Get(admindata, "ValueProposalPhrase").String()
	AEC.WebPage = gjson.Get(admindata, "Webpage").String()
	AEC.Logo = gjson.Get(admindata, "Logo").String()
	AEC.Services = GetServicesJsonFromAdminData(admindata)
	AEC.Promotions = GetPromotionsFromAdminData(admindata)
	AEC.ConditionPromotions = gjson.Get(admindata, "PromotionCondition").String()
	AEC.ExperienceQuality = true
	AEC.ContactName = gjson.Get(admindata, "ContactName").String()
	AEC.Email = gjson.Get(admindata, "ContactEmail").String()
	AEC.Phone = gjson.Get(admindata, "ContactPhone").String()
	AEC.Questions = gjson.Get(admindata, "Questions").String()
	AEC.CoverageArea = GetCoverageAreaFromAdminData(admindata)

	return AEC, nil
}

func GetJsonResponseFromStrapiJson(strapidata string) ([]byte, error) {
	data := gjson.Get(strapidata, "data")
	dataArray := data.Array()
	AdminCards := make([]AdminEntryCard, len(dataArray))
	var err error

	for i := 0; i < len(dataArray); i++ {
		cardData := dataArray[i].String()
		AdminCards[i], err = GetAdminCardFromJson(cardData)
		if err != nil {
			return []byte(""), err
		}

	}

	out, _ := json.Marshal(AdminCards)

	return out, nil
}

func GetCoverageAreaFromAdminData(admindata string) []KeyName {
	zones := gjson.Get(gjson.Get(admindata, "CoverageArea").String(), "Zones").Array()
	var coverageAreaOut []KeyName

	for i := 0; i < len(zones); i++ {
		upcns := UpperCaseNoSpaceStr(gjson.Get(zones[i].String(), "Zone").String())
		coverageAreaOut = append(coverageAreaOut, KeyName{Key: upcns, Name: Communes[upcns]})
	}

	return coverageAreaOut
}

func GetServicesJsonFromAdminData(admindata string) []KeyName {
	services := gjson.Get(admindata, "Services")
	var serviceout []KeyName
	offer := gjson.Get(services.String(), "FotografiaProfesional").String()
	if offer == "pagado" || offer == "servicio básico" {
		serviceout = append(serviceout, KeyName{Key: "FOTOGRAFIA", Name: ServiciosOfertados["FOTOGRAFIA"]})
	}
	offer = gjson.Get(services.String(), "PublicacionMultiportal").String()
	if offer == "pagado" || offer == "servicio básico" {
		serviceout = append(serviceout, KeyName{Key: "PUBLICACION", Name: ServiciosOfertados["PUBLICACION"]})
	}
	offer = gjson.Get(services.String(), "VisitasPeriodicas").String()
	if offer == "pagado" || offer == "servicio básico" {
		serviceout = append(serviceout, KeyName{Key: "VISITAS", Name: ServiciosOfertados["VISITAS"]})
	}
	offer = gjson.Get(services.String(), "RevisionRecepcion").String()
	if offer == "pagado" || offer == "servicio básico" {
		serviceout = append(serviceout, KeyName{Key: "REVISION", Name: ServiciosOfertados["REVISION"]})
	}
	offer = gjson.Get(services.String(), "DecoracionAmoblado").String()
	if offer == "pagado" || offer == "servicio básico" {
		serviceout = append(serviceout, KeyName{Key: "DECORACION", Name: ServiciosOfertados["DECORACION"]})
	}
	offer = gjson.Get(services.String(), "AsesoriaLegal").String()
	if offer == "pagado" || offer == "servicio básico" {
		serviceout = append(serviceout, KeyName{Key: "ASESORIA", Name: ServiciosOfertados["ASESORIA"]})
	}
	offer = gjson.Get(services.String(), "SegurosGarantia").String()
	if offer == "pagado" || offer == "servicio básico" {
		serviceout = append(serviceout, KeyName{Key: "SEGUROS", Name: ServiciosOfertados["SEGUROS"]})
	}
	offer = gjson.Get(services.String(), "FirmaDigital").String()
	if offer == "pagado" || offer == "servicio básico" {
		serviceout = append(serviceout, KeyName{Key: "FIRMA", Name: ServiciosOfertados["FIRMA"]})
	}

	return serviceout
}

func GetPromotionsFromAdminData(admindata string) []string {
	var promotions []string
	prom1 := gjson.Get(admindata, "Promotion").String()
	if prom1 != "Otra" {
		promotions = append(promotions, prom1)
	}
	otherproms := gjson.Get(admindata, "OtherPromotions").Array()
	for i := 0; i < len(otherproms); i++ {
		promotions = append(promotions, gjson.Get(otherproms[i].String(), "Promotion").String())
	}
	return promotions
}

func UpperCaseNoSpaceStr(instr string) string {
	instr = strings.ReplaceAll(strings.ToLower(instr), "ñ", "n")
	instr = strings.ToUpper(instr)
	instr = strings.ReplaceAll(instr, " ", "_")

	return instr
}
