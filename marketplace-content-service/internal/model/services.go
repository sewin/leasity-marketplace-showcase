package model

var ServiciosOfertados = map[string]string{
	"FOTOGRAFIA":  "Fotografía profesional",
	"PUBLICACION": "Publicación Multiportales",
	"VISITAS":     "Visitas periódicas a la propiedad",
	"REVISION":    "Revisión y recepción de propiedades nuevas",
	"ASESORIA":    "Asesoría legal (para desalojos, subsidios, etc)",
	"FIRMA":       "Firma digital de contrato",
	"SEGUROS":     "Seguros de garantía o ante no pago",
	"DECORACION":  "Servicio de decoración y amoblado",
}
