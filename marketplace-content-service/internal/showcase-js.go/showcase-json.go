package showcasejsgo

import (
	"bytes"
	"sync"
)

type MarketplaceShowcaseInfo struct {
	Body    []byte
	b       bytes.Buffer
	Updated bool
	m       sync.RWMutex
}

func (msi *MarketplaceShowcaseInfo) GetBody() []byte {
	msi.m.RLock()
	out := msi.b.Bytes()
	msi.m.RUnlock()
	return out
}

func (msi *MarketplaceShowcaseInfo) UpdateBody(newBody []byte) (int, error) {
	msi.m.Lock()
	n, err := msi.b.Write(newBody)
	msi.Updated = true
	msi.m.Unlock()
	return n, err
}

func (msi *MarketplaceShowcaseInfo) DeleteBodyCache() error {
	msi.m.Lock()
	msi.b.Reset()
	msi.Updated = false
	msi.m.Unlock()
	return nil
}

func NewMarketplaceShowCase(n int) (*MarketplaceShowcaseInfo, error) {
	b := bytes.NewBuffer(make([]byte, n))

	msi := MarketplaceShowcaseInfo{b: *b, Updated: false}

	return &msi, nil
}
