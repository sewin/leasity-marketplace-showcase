module leasity/marketplace-content-service

go 1.17

require (
	github.com/fasthttp/router v1.4.12
	github.com/tidwall/gjson v1.14.3
	github.com/valyala/fasthttp v1.40.0
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/klauspost/compress v1.15.0 // indirect
	github.com/savsgio/gotils v0.0.0-20220530130905-52f3993e8d6d // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
