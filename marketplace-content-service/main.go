package main

import (
	"leasity/marketplace-content-service/server"
	"os"
)

func main() {

	os.Setenv("MARKETPLACE_STRAPI_URL", "http://localhost:1337")

	fserver := server.NewFastHttpServer("0.0.0.0:8080")
	fserver.ListenAddr = "0.0.0.0:8080"
	fserver.Run()

}
