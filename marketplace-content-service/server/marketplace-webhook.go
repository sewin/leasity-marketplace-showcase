package server

import (
	"log"

	"github.com/valyala/fasthttp"
)

func NewPublishedContent(ctx *fasthttp.RequestCtx) {
	log.Println("Reseting Buff")
	err := MarketplaceShowase.DeleteBodyCache()
	if err != nil {
		ctx.Error("Server Error", 500)
		return
	}
	ctx.SetStatusCode(200)
}
