package server

import (
	"leasity/marketplace-content-service/internal/model"
	"log"
	"os"

	"github.com/valyala/fasthttp"
)

func MarketplaceEntries(ctx *fasthttp.RequestCtx) {
	log.Println("Looking for marketplace entries")
	if !MarketplaceShowase.Updated {

		strapiurl := os.Getenv("MARKETPLACE_STRAPI_URL")
		strapiurl += "/api/admin-marketplace-entries?populate[Services]=*&populate[CoverageArea][populate][1]=Zones&populate[OtherPromotions]=*"
		authToken := string(ctx.Request.Header.Peek("Authorization"))
		if authToken == "" {
			ctx.Error("No authorization", 404)
			return
		}
		contentreq := fasthttp.AcquireRequest()
		defer fasthttp.ReleaseRequest(contentreq)
		contentreq.Header.Set("Authorization", "Bearer "+authToken)
		contentreq.Header.SetMethod("GET")
		contentreq.SetRequestURI(strapiurl)

		contentres := fasthttp.AcquireResponse()
		defer fasthttp.ReleaseResponse(contentres)

		if err := fasthttp.Do(contentreq, contentres); err != nil {
			ctx.Error(err.Error(), 400)
			return
		}

		out, err := model.GetJsonResponseFromStrapiJson(string(contentres.Body()))
		if err != nil {
			return
		}

		n, err := MarketplaceShowase.UpdateBody(out)

		if err != nil {
			log.Println(err.Error())
			ctx.Error("Server error", 500)
			return
		}

		log.Println(n, " bytes written: ", string(out))
	}

	ctx.SetContentType("application/json")
	ctx.Write(MarketplaceShowase.GetBody())
}
