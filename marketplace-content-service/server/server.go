package server

import (
	"log"

	showcasejsgo "leasity/marketplace-content-service/internal/showcase-js.go"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

type FastHTTPServer struct {
	ListenAddr string
	Router     *router.Router
}

var MarketplaceShowase *showcasejsgo.MarketplaceShowcaseInfo

func NewFastHttpServer(addr string) *FastHTTPServer {
	r := router.New()

	return &FastHTTPServer{ListenAddr: addr, Router: r}
}

func (s *FastHTTPServer) Run() {
	log.Println("server listening")

	var err error

	MarketplaceShowase, err = showcasejsgo.NewMarketplaceShowCase(100000)

	if err != nil {
		log.Println(err.Error())
		return
	}

	MarketplaceShowase.DeleteBodyCache()

	if err != nil {
		log.Println(err.Error())
		return
	}

	s.Router.POST("/new-published-content", NewPublishedContent)
	s.Router.GET("/marketplace-admin-entries", MarketplaceEntries)

	if err = fasthttp.ListenAndServe(s.ListenAddr, s.Router.Handler); err != nil {
		log.Fatalf("error in ListenAndServe: %s", err)
	}

}
