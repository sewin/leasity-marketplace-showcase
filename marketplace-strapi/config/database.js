const path = require('path');

module.exports = ({ env }) => ({
  connection: {
    client: 'postgres',
    connection: {
      host: env('DATABASE_HOST', '172.17.0.4'),
      port: env.int('DATABASE_PORT', 5432),
      database: env('DATABASE_NAME', 'marketplace-showcase'),
      user: env('DATABASE_USERNAME', 'leasity'),
      password: env('DATABASE_PASSWORD', 'leasity123'),
      schema: env('DATABASE_SCHEMA', 'public'), // Not required
      ssl: false
    },
    debug: false,
  },
});
