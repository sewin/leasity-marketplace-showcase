'use strict';

/**
 *  admin-marketplace-entry controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::admin-marketplace-entry.admin-marketplace-entry');
