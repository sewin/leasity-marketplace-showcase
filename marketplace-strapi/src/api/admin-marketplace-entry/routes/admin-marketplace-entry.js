'use strict';

/**
 * admin-marketplace-entry router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::admin-marketplace-entry.admin-marketplace-entry');
