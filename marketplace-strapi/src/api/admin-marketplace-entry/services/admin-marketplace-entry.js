'use strict';

/**
 * admin-marketplace-entry service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::admin-marketplace-entry.admin-marketplace-entry');
