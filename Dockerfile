#### 

FROM golang:1.17 as builder

WORKDIR /go/src
COPY ./marketplace-content-service ./
 
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /go/bin/start .

#### 

FROM strapi/base

WORKDIR /app

RUN apt-get update && apt-get install -y supervisor
RUN mkdir -p /var/log/supervisor
RUN mkdir -p /app/samples

COPY ./marketplace-strapi/config ./config
COPY ./marketplace-strapi/src ./src
COPY ./marketplace-strapi/package.json ./package.json
COPY ./marketplace-strapi/public ./public

COPY --from=builder /go/bin .
# COPY ./newsletter-service/templates ./templates

RUN npm install

ENV NODE_ENV production

RUN npm run build
RUN chmod +x ./start

EXPOSE 1337

COPY supervisordocker.conf /etc/supervisor/conf.d/supervisord.conf

CMD ["/usr/bin/supervisord","-n","-c","/etc/supervisor/conf.d/supervisord.conf"]
